package grafico;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Aluno
 */
public class TelaInicialController implements Initializable {

    @FXML
    private TextField escreve;
    @FXML
    private Label fa;
    @FXML
    private Label ce;
    @FXML
    private Label ke;

    @FXML
    private Button coizito;

    private String letra;
    private int x, c, k;
    private double f;

    @FXML
    private void cel() {

        letra = escreve.getText();
        x = Integer.parseInt(letra);
        k= x + 273;
        
        f = 1.8 * x + 32;

        fa.setText("resultado em Fahrenheit:" + f);
        ce.setText("resultado em Celsius :" + x);
        ke.setText("resultado em kelvin:" + k);

    }

    @FXML
    private void kel() {
        letra = escreve.getText();
        x = Integer.parseInt(letra);
        
        c= x - 273;
        f=x*1.8-459.67;
        
        ce.setText("resultado em celsus:" + c);
        ke.setText("resultado em kelvin:" + x);
        fa.setText("resultado em Fahrenheit:" + f);
        
    }

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
